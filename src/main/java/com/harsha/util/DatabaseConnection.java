package com.harsha.util;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseConnection {

    public static Connection getConnection() throws SQLException {
        Connection connection = null;

        try {
            InitialContext context = new InitialContext();
            DataSource pool = (DataSource) context.lookup("java:comp/env/jdbc/contactus_db");
            connection = pool.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return connection;
    }
}

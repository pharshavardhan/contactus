package com.harsha.servlet;

import com.harsha.dao.UserDao;
import com.harsha.model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "LoginServlet", value = "/admin/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        HttpSession session = servletRequest.getSession();
        if (session.getAttribute("userName") != null) {
            servletResponse.sendRedirect("dashboard");
        } else {
            RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("login.jsp");
            requestDispatcher.forward(servletRequest, servletResponse);
        }
    }

    @Override
    protected void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        User user = new User();
        user.setUserName(servletRequest.getParameter("userName"));
        user.setPassword(servletRequest.getParameter("password"));

        if (UserDao.isValidUser(user)) {
            HttpSession session = servletRequest.getSession();
            session.setAttribute("userName", user.getUserName());
            servletResponse.sendRedirect("dashboard");
        } else {
            servletRequest.setAttribute("status", "error");
            RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("login.jsp");
            requestDispatcher.forward(servletRequest, servletResponse);
        }
    }
}

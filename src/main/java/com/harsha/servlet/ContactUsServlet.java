package com.harsha.servlet;

import com.harsha.dao.RequestDao;
import com.harsha.model.Request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ContactUsServlet", value = "/contactus")
public class ContactUsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("contactus.jsp");
        requestDispatcher.forward(servletRequest, servletResponse);
    }

    @Override
    protected void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        Request request = new Request();
        request.setFullName(servletRequest.getParameter("fullName"));
        request.setEmail(servletRequest.getParameter("email"));
        request.setMessage(servletRequest.getParameter("message"));

        String status = RequestDao.saveRequest(request) ? "success" : "error";
        servletRequest.setAttribute("status", status);

        RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("contactus.jsp");
        requestDispatcher.forward(servletRequest, servletResponse);
    }
}

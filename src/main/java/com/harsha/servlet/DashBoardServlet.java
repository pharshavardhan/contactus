package com.harsha.servlet;

import com.harsha.dao.RequestDao;
import com.harsha.model.Request;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "DashBoardServlet", value = "/admin/dashboard")
public class DashBoardServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        HttpSession session = servletRequest.getSession();

        if (session.getAttribute("userName") != null) {
            Map<String, List<Request>> userRequests = RequestDao.getRequests();
            servletRequest.setAttribute("userRequests", userRequests);
            RequestDispatcher requestDispatcher = servletRequest.getRequestDispatcher("dashboard.jsp");
            requestDispatcher.forward(servletRequest, servletResponse);
        } else {
            servletResponse.sendRedirect("login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        int requestId = Integer.parseInt(servletRequest.getParameter("requestId"));

        if (RequestDao.archiveRequest(requestId)) {
            doGet(servletRequest, servletResponse);
        }
    }
}

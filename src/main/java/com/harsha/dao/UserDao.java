package com.harsha.dao;

import com.harsha.model.User;
import com.harsha.util.DatabaseConnection;

import java.sql.*;

public class UserDao {
    public static boolean isValidUser(User user) {
        final String query = "select password from users where user_name = ?";

        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, user.getUserName());
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next() && user.getPassword().equals(resultSet.getString("password"))) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

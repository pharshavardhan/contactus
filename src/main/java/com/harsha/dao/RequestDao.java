package com.harsha.dao;

import com.harsha.model.Request;
import com.harsha.util.DatabaseConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestDao {
    public static boolean saveRequest(Request request) {
        final String query = "insert into requests(full_name, email, message, is_active) values(?, ?, ?, ?)";

        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, request.getFullName());
            statement.setString(2, request.getEmail());
            statement.setString(3, request.getMessage());
            statement.setBoolean(4, true);
            statement.execute();

            return true;
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Map<String, List<Request>> getRequests() {
        final String query = "select * from requests";
        Map<String, List<Request>> requests = new HashMap<>();
        List<Request> activeRequests = new ArrayList<>();
        List<Request> archiveRequests = new ArrayList<>();

        try (Connection connection = DatabaseConnection.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Request request = new Request();
                request.setId(resultSet.getInt("id"));
                request.setFullName(resultSet.getString("full_name"));
                request.setEmail(resultSet.getString("email"));
                request.setMessage(resultSet.getString("message"));
                request.setActive(resultSet.getBoolean("is_active"));

                if (request.isActive()) {
                    activeRequests.add(request);
                } else {
                    archiveRequests.add(request);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        requests.put("active", activeRequests);
        requests.put("archive", archiveRequests);
        return requests;
    }

    public static boolean archiveRequest(int requestId) {
        final String query = "update requests set is_active = 0 where id = ?";

        try (Connection connection = DatabaseConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, requestId);
            statement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}

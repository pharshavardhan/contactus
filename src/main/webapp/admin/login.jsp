<%--
  Created by IntelliJ IDEA.
  User: harsha
  Date: 05/03/22
  Time: 12:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="login.css" type="text/css">
</head>
<body>
    <div class="container">
        <header class="header">
            <h1>Login</h1>
            <p>Login to view request details</p>
        </header>

        <c:set var="status" value="${pageContext.request.getAttribute(\"status\")}"/>
        <c:if test="${\"error\".equalsIgnoreCase(status)}">
            <span class="error">Invalid credentials</span>
        </c:if>

        <form method="post">
            <div class="form-element">
                <label for="userName">User Name<sup>*</sup></label>
                <input class="input-text" type="text" name="userName" id="userName" placeholder="Enter your user name" required>
            </div>
            <div class="form-element">
                <label for="password">Password<sup>*</sup></label>
                <input class="input-text" type="password" name="password" id="password" placeholder="Enter your password" required>
            </div>
            <div class="form-element">
                <button type="submit">Login</button>
            </div>
        </form>
    </div>
</body>
</html>

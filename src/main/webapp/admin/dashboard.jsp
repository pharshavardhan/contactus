<%--
  Created by IntelliJ IDEA.
  User: harsha
  Date: 05/03/22
  Time: 2:02 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.Map, java.util.List, com.harsha.model.Request, java.lang.String" %>
<html>
<head>
    <title>Dash Board</title>
    <link rel="stylesheet" href="dashboard.css" type="text/css">
</head>
<body>
    <c:set var="userRequests" value="<%=(Map<String, List<Request>>) request.getAttribute(\"userRequests\")%>"/>
    <c:set var="activeRequests" value="${userRequests.get(\"active\")}"/>
    <c:set var="archiveRequests" value="${userRequests.get(\"archive\")}"/>
    
    <section class="container activeReq">
        <h2>Active Requests</h2>
        
        <c:choose>
            <c:when test="${activeRequests.size() == 0}">
                <p class="noRequests">All requests processed. Nothing to show</p>
            </c:when>
            <c:otherwise>
                <ul>
                    <li>
                        <span class="id"><b>Id</b></span>
                        <span class="name"><b>Name</b></span>
                        <span class="email"><b>Email</b></span>
                        <span class="message"><b>Message</b></span>
                    </li>
                </ul>
                <c:forEach var="userRequest" items="${activeRequests}">
                    <ul>
                        <li>
                            <span class="id">${userRequest.getId()}</span>
                            <span class="name">${userRequest.getFullName()}</span>
                            <span class="email">${userRequest.getEmail()}</span>
                            <span class="message">${userRequest.getMessage()}</span>
                            <form method="post" class="archiveBtn">
                                <input type="hidden" name="requestId" value="${userRequest.getId()}">
                                <button type="submit">Archive</button>
                            </form>
                        </li>
                    </ul>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </section>

    <section class="container archiveReq">
        <h2>Archived Requests</h2>

        <ul>
            <li>
                <span class="id"><b>Id</b></span>
                <span class="name"><b>Name</b></span>
                <span class="email"><b>Email</b></span>
                <span class="message"><b>Message</b></span>
            </li>
        </ul>
        <c:forEach var="userRequest" items="${archiveRequests}">
            <ul>
                <li>
                    <span class="id">${userRequest.getId()}</span>
                    <span class="name">${userRequest.getFullName()}</span>
                    <span class="email">${userRequest.getEmail()}</span>
                    <span class="message">${userRequest.getMessage()}</span>
                </li>
            </ul>
        </c:forEach>
    </section>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: harsha
  Date: 04/03/22
  Time: 7:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Contact Us</title>
    <link rel="stylesheet" href="contactus.css" type="text/css">
</head>
<body>
    <div class="container">
        <header class="header">
            <h1>Contact Us</h1>
            <p>Please fill this form in a decent manner</p>
        </header>

        <c:set var="status" value="${pageContext.request.getAttribute(\"status\")}"/>
        <c:choose>
            <c:when test="${\"success\".equalsIgnoreCase(status)}">
                <span class="success">Your request has been saved. We will get back to you</span>
            </c:when>
            <c:when test="${\"error\".equalsIgnoreCase(status)}">
                <span class="error">There was an error. Please try again</span>
            </c:when>
        </c:choose>

        <form method="post">
            <div class="form-element">
                <label for="fullName">Full Name<sup>*</sup></label>
                <input class="input-text" type="text" name="fullName" id="fullName" placeholder="Enter your full name" required>
            </div>
            <div class="form-element">
                <label for="email">E-Mail<sup>*</sup></label>
                <input class="input-text" type="email" name="email" id="email" placeholder="Enter your email" required>
            </div>
            <div class="form-element">
                <label for="message">Message<sup>*</sup></label>
                <textarea class="input-textarea" name="message" id="message" rows="4" cols="40" maxlength="400" placeholder="Max 400 characters" required></textarea>
            </div>
            <div class="form-element">
                <button type="submit">Submit</button>
            </div>
        </form>
    </div>
</body>
</html>
